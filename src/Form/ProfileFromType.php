<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\DBAL\Types\BooleanType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProfileFromType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('email', EmailType::class, [
                'required' => true,
                'mapped' => true,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Proszę podać adres email4'
                    ]),
                    new Email([
                        'message' => 'Proszę podać poprawny adres e-mail'
                    ])
                ]
            ])
            ->add('password', PasswordType::class, [
                'error_bubbling' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Proszę podać hasło.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('repeatPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Proszę podać ponownie hasło.',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('name', TextType::class, [
                'mapped' => true,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Proszę podać poprawne imię'
                    ])
                ]
            ])
            ->add('surname', TextType::class, [
                'mapped' => true,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Proszę podać poprawne nazwisko.'
                    ])
                ]
            ])
            ->add('isVerified', CheckboxType::class, [
                'mapped' => true,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Należy zaakceptować zgodzę serwisu'
                    ])
                ]
            ])
            ->add('deleted', CheckboxType::class, [
                'mapped' => false,
                'empty_data' => '',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Zła wartość'
                    ])
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, static function (FormEvent $event) {
            /**
             * @var array $profile
             */
            $profile = $event->getData();
            $password = $profile['password'] ?? false;
            $repeatPassword = $profile['repeatPassword'] ?? false;
            if (($password && $repeatPassword && $password !== $repeatPassword) || ($password && !$repeatPassword)) {
                $event->getForm()->get('password')
                    ->addError(new FormError('Podane hasła różnią się. Proszę wprowadzić ponownie. '));
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false
        ]);
    }
}
