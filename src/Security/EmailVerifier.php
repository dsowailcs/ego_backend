<?php

namespace App\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class EmailVerifier
{
    private $verifyEmailHelper;
    private $mailer;
    private $entityManager;
    private $generator;

    public function __construct(VerifyEmailHelperInterface $helper, MailerInterface $mailer,
                                EntityManagerInterface $manager, UrlGeneratorInterface $generator)
    {
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        $this->entityManager = $manager;
        $this->generator = $generator;
    }

    public function sendEmailConfirmation(string $verifyEmailRouteName, $activationLink, TemplatedEmail $email): void
    {
        $link = $this->generator->generate($verifyEmailRouteName, [
            'activationLink' => $activationLink
        ],
            UrlGeneratorInterface::ABSOLUTE_URL);
        $context['signedUrl'] = $link;
        $context['expiresAt'] = 12;

        $email->context($context);

        try {
            @$this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }

    /**
     * @param Request $request
     * @param UserInterface $user
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, UserInterface $user): void
    {
        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getEmail());

        $user->setIsVerified(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function resetPasswordConfirmationMail(string $activationLink, TemplatedEmail $email): void
    {
        $context['signedUrl'] = $activationLink;
        $context['expiresAt'] = 12;

        $email->context($context);

        try {
            @$this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }

}
