<?php

namespace App\Security;

use App\Entity\User;
use App\Entity\User as AppUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $request;

    public function __construct(EntityManagerInterface $em, RequestStack $request)
    {
        $this->em = $em;
        $this->request = $request;
    }

    /**
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if (false === $user->isVerified()) {
        // the message passed to this exception is meant to be displayed to the user
            throw new CustomUserMessageAccountStatusException(
                'Twoje konto nie jest aktywne. Proszę wejść w link aktywacyjny wysłany na podany ' .
                'adres e-mail podczas rejestracji.'
            );
        }
        $route = $this->request->getCurrentRequest()->get('_route');
        if ($route === 'api_login') {
            $this->setCustomApiLoginWork($user);
        }
    }

    private function setCustomApiLoginWork(User $user): void
    {
        // Some work for ego frontend stuff.
    }
}
