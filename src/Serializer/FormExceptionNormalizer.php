<?php


namespace App\Serializer;


use App\Exception\FormException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FormExceptionNormalizer implements NormalizerInterface
{
    /**
     * @param FormException $exception
     * @param string $format
     * @param array $context
     *
     * @return array|bool|float|int|string|void
     */
    public function normalize($exception, string $format = null, array $context = [])
    {
        return self::getFormErrorsTreeArray($exception->getForm());
    }


    /**
     * @param mixed $data
     * @param string  $format
     *
     * @return bool|void
     */
    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof FormException;
    }
    public static function getFormErrorsTreeArray(FormInterface $form, $single = true): array
    {
        $errors = [];
        if (count($form->getErrors(false)) > 0) {
            foreach ($form->getErrors(true) as $error) {
                if($single === true) {
                    $errors[$error->getOrigin()->getName()] = [$error->getMessage()];
                }
                else {
                    $errors[] = $error->getMessage();
                }


            }
        } else {
            $single = false;
            foreach ($form->all() as $child) {
                $childTree = self::getFormErrorsTreeArray($child, $single);
                if (count($childTree) > 0) {
                    $errors[(string)$child->getName()] = $childTree;
                }
            }
        }
        return $errors;
    }
}
