<?php

namespace App\Controller;

use App\Entity\User;
use MongoDB\Driver\Exception\AuthenticationException;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login", methods={"GET","POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('admin_home');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("api/login", name="api_login", methods={"POST"})
     * @param JWTTokenManagerInterface $JWTTokenManager
     * @return Response
     */
    public function apiLogin(JWTTokenManagerInterface $JWTTokenManager): Response
    {
        /**
         * @var $user User
         */
        $user = $this->getUser();
        if($user) {
            return $this->json([
                'username' => $user->getUsername(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'roles' => $user->getRoles(),
                'token' => $user->getApiToken(),
                'jwt' => $JWTTokenManager->create($user),
                'id' => $user->getId()
            ]);
        }

        return $this->json(['error'=>'Login error']);

    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \RuntimeException('This method can be blank - it will be intercepted by the logout key on your firewall');
    }



}
