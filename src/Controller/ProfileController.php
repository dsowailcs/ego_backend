<?php

namespace App\Controller;

use App\Entity\User;
use App\Http\ApiResponse;
use App\Form\ProfileFromType;
use App\Exception\FormException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class ProfileController extends AbstractController
{

    /**
     * @Route("api/profile", name="profile_get", methods={"GET"})
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function getProfile(EntityManagerInterface $em): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        return new ApiResponse('Dane profilowe', [
            'user' => [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'email' => $user->getEmail()
            ]
        ]);
    }

    /**
     * @Route("api/profile", name="profile_patch", methods={"PATCH"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @return JsonResponse
     */
    public function updateProfile(Request $request, EntityManagerInterface $em,
                                  UserPasswordEncoderInterface $userPasswordEncoder): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();
        $form = $this->createForm(ProfileFromType::class, $user);
        $form->submit($request->request->all(), false);

        if (false === $form->isValid()) {
            throw new FormException($form);
        }
        // encode the plain password
        if ($form->get('password')->getData()) {
            $user->setPassword(
                $userPasswordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
        }
        if($form->get('deleted')->getData() === true) {
            $user->setDeleted(true);
        }
        $user->setUsername($form->get('email')->getData());
        $em->persist($user);
        $em->flush();

        return new ApiResponse('Aktualizacja przebiegła pomyślnie.', ['status' => 200]);
    }

}
