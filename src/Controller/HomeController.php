<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     * @ParamConverter("dateTime", options={"format": "!Y-m-d h:i:s"})
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function home(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('home.html.twig');
    }

    /**
     * @param $lastHourCheck
     * @return bool
     */

    private function isYesterday($lastHourCheck)
    {
        return date('Ymd', $lastHourCheck->getTimestamp()) === date('Ymd', strtotime('yesterday'));
    }
}
