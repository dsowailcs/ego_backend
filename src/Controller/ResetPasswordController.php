<?php


namespace App\Controller;

use App\Entity\User;
use App\Http\ApiResponse;
use App\Security\EmailVerifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ResetPasswordController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("api/password-reset-send-link", name="post_send_link", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $manager
     * @return JsonResponse
     */
    public function sendActivationLink(Request $request, ValidatorInterface $validator,
                                       EntityManagerInterface $manager): JsonResponse
    {

        $userEmail = $request->get('userEmail');
        $emailConstraint = new Email();
        $emailConstraint->message = 'Proszę podać poprawny adres e-mail';
        $notBlankConstraint = new NotBlank();
        $notBlankConstraint->message = 'Proszę podać adres e-mail';
        $errors = $validator->validate(
            $userEmail,
            [
                $emailConstraint,
                $notBlankConstraint
            ]
        );
        if ($errors->count()) {
            return new ApiResponse('Błąd', null, [
                'userEmail' => $errors->get(0)->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = $manager->getRepository(User::class)->findOneBy([
            'email' => $userEmail
        ]);
        if ($user) {
            $resetPasswordLink = $user->getResetPasswordLink() ?: Uuid::v6()->__toString();
            $user->setResetPasswordLink($resetPasswordLink);
            $manager->persist($user);
            $manager->flush();
            $this->emailVerifier->resetPasswordConfirmationMail(
                'https://www.akademiarodzicowbobolen.pl/auth/reset-password?resetPasswordLink=' . $resetPasswordLink,
                (new TemplatedEmail())
                    ->from(new Address('info@akademiarodzicowbobolen.pl', 'Akademia Rodziców Bobolen'))
                    ->to($user->getEmail())
                    ->subject('Przypomnienie hasła dla konta w serwisie Akademia Rodziców Bobolen')
                    ->htmlTemplate('security/password_reset.html.twig')
            );
            return new ApiResponse('E-mail z linkiem resetującym hasło został wysłany.');
        }
        return new ApiResponse('Nie znaleziono użytkownika o podanym adresie e-mail.', null, [], 400);
    }

    /**
     * @Route("api/reset-password", name="post_reset_password", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function resetPassword(Request $request, EntityManagerInterface $manager,
                                  ValidatorInterface $validator,
                                  UserPasswordEncoderInterface $passwordEncoder): JsonResponse
    {
        $link = $request->get('resetPasswordLink');
        $passwords = $request->get('password');
        $first = $passwords['first'];
        $second = $passwords['second'];
        $errors = $validator->validate(
            $first,
            [
                new NotBlank([
                    'message' => 'Proszę podać hasło.',
                ]),
                new Length([
                    'min' => 6,
                    'minMessage' => 'Nowe hasło powinno mieć co najmniej {{ limit }} znaków',
                    // max length allowed by Symfony for security reasons
                    'max' => 4096,
                ])
            ]
        );
        if ($errors->count()) {
            return new ApiResponse('Błąd', null, [
                'first' => $errors->get(0)->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        $errors = $validator->validate(
            $second,
            [
                new NotBlank([
                    'message' => 'Proszę podać hasło.',
                ]),
                new Length([
                    'min' => 6,
                    'minMessage' => 'Nowe hasło powinno mieć co najmniej {{ limit }} znaków',
                    // max length allowed by Symfony for security reasons
                    'max' => 4096,
                ])
            ]
        );
        if ($errors->count()) {
            return new ApiResponse('Błąd', null, [
                'second' => $errors->get(0)->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        $user = $manager->getRepository(User::class)->findOneBy(
            [
                'resetPasswordLink' => $link
            ]
        );
        if ($user) {
            if ($first !== $second) {
                return new ApiResponse('Podane hasła nie są identyczne!', null, [],
                    Response::HTTP_BAD_REQUEST);
            }
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $first
                )
            );
            $user->setResetPasswordLink(null);
            $manager->persist($user);
            $manager->flush();

            return new ApiResponse('Hasło zostało poprawnie zmienione, zaloguj się nowym hasłem do serwisu!');
        }


        return new ApiResponse('Wystąpił problem, hasło nie może zostać zmienione!', null, [],
            Response::HTTP_BAD_REQUEST);
    }

}
