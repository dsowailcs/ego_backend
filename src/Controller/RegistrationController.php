<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\User;
use App\Exception\FormException;
use App\Form\RegistrationFormType;
use App\Http\ApiResponse;
use App\Security\EmailVerifier;
use App\Security\JwtTokenAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Uid\Uuid;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("api/register", name="app_register", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param JwtTokenAuthenticator $authenticator
     * @return Response
     * @throws Exception
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder,
                             GuardAuthenticatorHandler $guardHandler,
                             JwtTokenAuthenticator $authenticator): Response
    {
        $user = new User();

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->submit($request->request->all());
        if(false === $form->isValid()) {
            throw new FormException($form);
        }
        // encode the plain password
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            )
        );
        $user->setRoles(['ROLE_USER']);
        $user->setDeleted(false);
        $user->setUsername($form->get('email')->getData());
        $user->setIsVerified(false);
        $activationLink = Uuid::v6()->__toString();
        $user->setActivationLink($activationLink);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        // generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation('app_verify_email', $activationLink,
            (new TemplatedEmail())
                ->from(new Address('info@akademiarodzicowbobolen.pl', 'Akademia Rodziców Bobolen'))
                ->to($user->getEmail())
                ->subject('Potwierdzenie konta Akademia Rodziców Bobolen')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );

        return new ApiResponse('Rejestracja przebiegła pomyślnie', ['status'=>200]);
    }

    /**
     * @Route("/verify/email", name="app_verify_email", methods={"GET"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function verifyUserEmail(Request $request, EntityManagerInterface $manager): Response
    {

        $activationLink = $request->query->get('activationLink');
        /**
         */
        $proposalUser = $manager->getRepository(User::class)->findOneBy([
            'activationLink' => $activationLink
        ]);
        if($proposalUser && $proposalUser->getActivationLink() === $activationLink) {
            $proposalUser->setIsVerified(true);
            $proposalUser->setActivationLink(null);
            $manager->persist($proposalUser);
            $manager->flush();
            return $this->redirect('https://www.akademiarodzicowbobolen.pl?activationLink=true');
        }

        return $this->redirect('https://www.akademiarodzicowbobolen.pl?activationLink=false');
    }
}
