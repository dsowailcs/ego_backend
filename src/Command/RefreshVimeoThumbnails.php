<?php


namespace App\Command;


use App\Entity\Material;
use App\Entity\User;
use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use function Doctrine\ORM\QueryBuilder;

class RefreshVimeoThumbnails extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:vimeo-thumbnails';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Connection $c, EntityManagerInterface $em,
                                string $name = null)
    {
        $this->em = $em;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Match and set known facilites for users')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create single files for elearnings, webinars and materials');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        ini_set('memory_limit', '5120M');
        $materials = $this->em->getRepository(Material::class)->findBy([
            'type' => Material::TYPE_VIDEO,
        //    'type' => Material::TYPE_PODCAST
        ]);
        /**
         * @var Material $material
         */
        foreach ($materials as $material) {
            $vimeoId = $material->getVimeoUrl();
            $thumb = file_get_contents('https://vimeo.com/api/oembed.json?url=https://vimeo.com/' . $vimeoId);
            $vimeoThumbnail = json_decode($thumb, true);
            $vimeoThumbnail = $vimeoThumbnail['thumbnail_url'];
            if('' !== $vimeoThumbnail) {
                $material->setThumbnailUrl($vimeoThumbnail);
                $this->em->persist($material);
            }
        }
        $this->em->flush();

        $output->writeln('Starting ...');
        return Command::SUCCESS;
    }


}
